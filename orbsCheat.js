knownOrbs = [ ];
currLevel = 1;
orbsCount = ( currLevel - 1 ) + 5;
checkedOrbs = [ ];
indexR = 0;
normal = false;

function clickOrb( index )
{
    $( ".orb" ).eq( index ).click();
}

function clickKnownOrbs()
{
    if ( knownOrbs.length === 0 )
    {
        return;
    }
    //checkedOrbs = [ ];
    for ( var i = 0; i < knownOrbs.length; i++ )
    {
        $( ".orb" ).eq( knownOrbs[i] ).click();
        //console.log( "normalClick" );
    }
}

function work() {
    if ( knownOrbs.length < orbsCount )
    {
        //checkedOrbs = [ ];
        clickKnownOrbs();
        setTimeout( function() {
            clickRandomVisibleOrb( orbsCount );
        }, 1000 );
    }
    else
    {
        for ( var i = 0; i < knownOrbs.length; i++ )
        {
            clickOrb( knownOrbs[i] );
        }
    }
}

function clickRandomVisibleOrb( orbsCount )
{
    do
    {
        if ( checkedOrbs.length >= orbsCount )
        {
            checkedOrbs.length = 0;
        }
        indexR = Math.round( Math.random() * orbsCount );
    }
    while ( $( ".orb" ).eq( indexR ).attr(
            "added" ) === "1" || $.inArray( indexR, checkedOrbs ) !== -1 );
    if ( knownOrbs.length > 0 ) {
        if ( $( ".orb" ).eq( knownOrbs[0] ).attr( "class" ) === "orb lit" )
        {
            normal = true;
        }
        else
        {
            normal = false;
        }
    }
    $( ".orb" ).eq( indexR ).click();
    setTimeout( check, 500 );
}

function check()
{
    if ( $( ".orb" ).eq( indexR ).attr( "class" ) === "orb lit" )
    {
        $( ".orb" ).eq( indexR ).click();
        if ( $.inArray( indexR, knownOrbs ) === -1 )
        {
            knownOrbs.push( indexR );
        }
        //checkedOrbs.length = 0;
        checkedOrbs = [ ];
        $( ".orb" ).eq( indexR ).attr( "added", "1" );
    }
    else
    {
        if ( knownOrbs.length > 0 )
        {
            if ( normal )
            {
                checkedOrbs.push( indexR );
            }
        }
        else
        {
            checkedOrbs.push( indexR );
        }

    }
    setTimeout( work, 1000 );
}

work();